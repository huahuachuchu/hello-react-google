import React from 'react';
import { MdSearch } from "react-icons/md";
import { MdKeyboardVoice } from "react-icons/md";
import './Input.less'


const Input = () => {
  return(
    <section className='input'>
      <MdSearch className="search-icon" />
      <input className='search-box'/>
      <MdKeyboardVoice className="voice-icon" />
    </section>
  );
};

export default Input;
