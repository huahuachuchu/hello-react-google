import React from 'react';
import './Search.less';
import Input from './Input';
import Button from './Button.less';

const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                   alt="Google Logo"/>
      </header>
      <Input />
      <button className='one'>Google 搜索</button>
      <button className='two'>手气不错</button>
      <div>
        Google 提供：
        <a className='chinese'>中文（繁體) </a>
        <a className='english'>English</a>
      </div>
    </section>
  );
};
export default Search;