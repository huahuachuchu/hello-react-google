import React from "react";
import './Footer.less';

const Footer = () => {
  return (
    <div className='ground'>
      <section className='part' id='part1'>
        <label>香港</label>
      </section>
      <section className='part' id='part2'>
        <ul>
          <li><a>广告</a></li>
          <li className='left'><a id='a_1'>商务</a></li>
          <li className='left'><a id='a_2'>Google 大全</a></li>
          <li className='left'><a id='a_3'>Google搜索的运作方式</a></li>
          <li className='right'><a id='a_4'>隐私权</a></li>
          <li className='right'><a id='a_5'>条款</a></li>
          <li className='right'><a id='a_6'>设置</a></li>
        </ul>
      </section>
    </div>
  );
};

export default Footer;